package webflash

import (
	"encoding/base64"
	"net/http"
	"time"
)

var (
	// CookieDomain should be set at some point during runtime initialization to ensure the flash cookies
	// are set for the correct domain.
	CookieDomain = ""
)

// Set will add a flash cookie to the http.ResponseWriter
func Set(w http.ResponseWriter, name string, value []byte) {
	c := &http.Cookie{Name: name, Value: encode(value), Domain: CookieDomain, Path: "/"}
	http.SetCookie(w, c)
}

// SetString is the same as Set, but accepts a string value instead of []byte
func SetString(w http.ResponseWriter, name string, value string) {
	Set(w, name, []byte(value))
}

// Get will check the http.Request for the named flash cookie, and return if it's found
// If the cookie doesn't exist at all, nil is returned for both the []byte and error, error is only
// returned non-nil if there is some other problem reading the cookie from the request.
func Get(w http.ResponseWriter, r *http.Request, name string) ([]byte, error) {
	c, err := r.Cookie(name)
	if err != nil {
		switch err {
		case http.ErrNoCookie:
			return nil, nil
		default:
			return nil, err
		}
	}
	value, err := decode(c.Value)
	if err != nil {
		return nil, err
	}
	dc := &http.Cookie{Name: name, MaxAge: -1, Expires: time.Unix(1, 0), Domain: CookieDomain, Path: "/"}
	http.SetCookie(w, dc)
	return value, nil
}

// GetString is the same as Get, but returns a string instead of a []byte
func GetString(w http.ResponseWriter, r *http.Request, name string) (string, error) {
	ret, err := Get(w, r, name)
	if ret == nil {
		return "", err
	}
	return string(ret), err
}

func encode(src []byte) string {
	return base64.URLEncoding.EncodeToString(src)
}

func decode(src string) ([]byte, error) {
	return base64.URLEncoding.DecodeString(src)
}
