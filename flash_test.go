package webflash

import (
	"encoding/base64"
	"net/http"
	"net/http/httptest"
	"testing"
	"time"

	"github.com/stretchr/testify/assert"
)

var (
	name      = "testingA"
	value     = "foo-A"
	unsetTime = time.Unix(1, 0).In(time.UTC)
)

func TestWebFlash_ByteSlice(t *testing.T) {
	assert := assert.New(t)

	// We create a ResponseRecorder (which satisfies http.ResponseWriter) to record the response.
	rr := httptest.NewRecorder()

	Set(rr, name, []byte(value))

	cookie := rr.Result().Cookies()[0]
	assert.Equal(name, cookie.Name)
	assert.Equal(base64.URLEncoding.EncodeToString([]byte(value)), cookie.Value)
	assert.Nil(cookie.Valid())

	// reset the recorder so we can re-use it
	rr = httptest.NewRecorder()

	// Copy the Cookie over to a new Request
	request := &http.Request{Header: http.Header{}}
	request.AddCookie(cookie)

	result, err := Get(rr, request, name)
	assert.Nil(err)
	assert.Equal([]byte(value), result)

	// Check that we've unset the cookie
	cookie = rr.Result().Cookies()[0]
	assert.Equal(-1, cookie.MaxAge)
	assert.Equal(unsetTime, cookie.Expires)
	assert.Equal(name, cookie.Name)
}

func TestWebFlash_String(t *testing.T) {
	assert := assert.New(t)

	// We create a ResponseRecorder (which satisfies http.ResponseWriter) to record the response.
	rr := httptest.NewRecorder()

	SetString(rr, name, value)

	cookie := rr.Result().Cookies()[0]
	assert.Equal(name, cookie.Name)
	assert.Equal(base64.URLEncoding.EncodeToString([]byte(value)), cookie.Value)
	assert.Nil(cookie.Valid())

	// reset the recorder so we can re-use it
	rr = httptest.NewRecorder()

	// Copy the Cookie over to a new Request
	request := &http.Request{Header: http.Header{}}
	request.AddCookie(cookie)

	result, err := GetString(rr, request, name)
	assert.Nil(err)
	assert.Equal(value, result)

	// Check that we've unset the cookie
	cookie = rr.Result().Cookies()[0]
	assert.Equal(-1, cookie.MaxAge)
	assert.Equal(unsetTime, cookie.Expires)
	assert.Equal(name, cookie.Name)
}

func TestWebFlash_CookieDoesntExist(t *testing.T) {
	assert := assert.New(t)

	rr := httptest.NewRecorder()
	request := &http.Request{Header: http.Header{}}

	value, err := GetString(rr, request, name)
	assert.Equal("", value)
	assert.Nil(err)
}
