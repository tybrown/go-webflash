# go-webflash

[![GoDoc](https://img.shields.io/badge/pkg.go.dev-doc-blue)](http://pkg.go.dev/gitlab.com/tybrown/go-webflash)
[![Go Report Card](https://goreportcard.com/badge/gitlab.com/tybrown/go-webflash)](https://goreportcard.com/report/gitlab.com/tybrown/go-webflash)
[![pipeline status](https://gitlab.com/tybrown/go-webflash/badges/main/pipeline.svg)](https://gitlab.com/tybrown/go-webflash/-/commits/main)
[![coverage report](https://gitlab.com/tybrown/go-webflash/badges/main/coverage.svg)](https://gitlab.com/tybrown/go-webflash/-/graphs/main/charts)
[![Latest Release](https://gitlab.com/tybrown/go-webflash/-/badges/release.svg)](https://gitlab.com/tybrown/go-webflash/-/releases/permalink/latest)

A library for setting and getting flash messages in web applications using temporary cookies.

## Usage

```go
func postHandler(w http.ResponseWriter, r *http.Request) {
    // do stuff

    webflash.SetString(w, "status-message", "Foo was successful")
    http.Redirect(w, r, "/foo", http.StatusFound)
}

func getHandler(w http.ResponseWriter, r *http.Request) {
    statusMsg, _ := webflash.GetString(w, r, "status-message")

    // do something with the status message if it's not empty
}
```
